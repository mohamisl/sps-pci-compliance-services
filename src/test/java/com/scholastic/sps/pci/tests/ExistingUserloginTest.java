package com.scholastic.sps.pci.tests;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.path.xml.XmlPath;
import org.junit.Assert;
import org.junit.Test;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class ExistingUserloginTest extends BaseTest
{
	String clientID;
	String isSingleToken;
	String userName;
	String password;
	
	final String ENDPOINT_PCICOMPLIANCE_AUTHENTICATION="/AuthenticatePCI/processRemote";
	

	@Test
	public void existingUserLoginWithNonPciCompliancePasswordTest() 
	{
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"gbari@juno1.com\" ";
		password=" \"pass\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xxx=existingUserLoginResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xxx);
		xmlPath.setRoot("SchWS");
		System.out.println(xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."));
	}
	
	
	@Test
	public void existingUserLoginWithPciCompliancePasswordTest() 
	{
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"jbari@juno1.com\" ";
		password=" \"passed1\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("Hu6FqRqYT4qfUOpAZpgacA=="));
	}
	
	@Test
	public void existingUserLogWithInvalidUserNameTest()
	{
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"jbari@sample.com\" ";
		password=" \"passed1\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("The account information you entered does not match our records."));
	}
	
	@Test
	public void existingUserLogWithInvalidPasswordTest()
	{
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"jbari@juno1.com\" ";
		password=" \"pass1\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("The account information you entered does not match our records."));
	}
	
	public String createQueryParam()
	{
		String queryParam=
				"<SchWS>"+
				"<attribute "+"name="+"\"clientID\" "+" value="+clientID+"/>"+
				"<attribute name="+"\"isSingleToken\" " +" value="+isSingleToken+"/>"+
				"<attribute name="+"\"userName\" " +" value="+userName+"/>"+
				"<attribute name="+"\"password\" " +" value="+password+"/>"+
				"</"+"SchWS"+">";
		return queryParam;
	}
}