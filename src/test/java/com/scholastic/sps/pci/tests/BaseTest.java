package com.scholastic.sps.pci.tests;

import static com.jayway.restassured.config.RestAssuredConfig.config;
import static com.jayway.restassured.config.SSLConfig.sslConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Before;
import org.junit.BeforeClass;

import com.jayway.restassured.RestAssured;

public class BaseTest 
{
	public static FileInputStream fis=null;
	public static Properties prop=null;
	public static String qaPropfilePath=System.getProperty("user.dir")+"/src/test/resources/qa.properties";
	public static String devPropfilePath=System.getProperty("user.dir")+"/src/test/resources/dev.properties";
	
	@BeforeClass
	public static void b4()
	{
		RestAssured.baseURI="http://schws.dev2.scholastic.net";
		RestAssured.port=80;
		RestAssured.basePath="/SchWS/services/SPS";
		RestAssured.config = config().sslConfig(sslConfig().allowAllHostnames());
		
		prop=new Properties();
		try
		{
				if(RestAssured.baseURI.contains("dev2"))
				{
					fis=new FileInputStream(devPropfilePath);
					prop.load(fis);
				}else
				{
					fis=new FileInputStream(qaPropfilePath);
					prop.load(fis);
				}
		}catch(IOException e)	
		{
			System.out.println("File Not Found");	
		}
		
	}	
	
	@Before
	public void initialization() 
	{
	System.out.println("\n\n\n\n*******Initialized RestAssured*******");
	System.out.println("RestAssured.baseURI: " + RestAssured.baseURI);
	System.out.println("RestAssured.port: " + RestAssured.port);
	System.out.println("RestAssured.basePath: " + RestAssured.basePath); 
	System.out.println("******************************\n\n\n\n");

	}
	
	public String readProperty(String key)
	{
		return prop.getProperty(key);
	}

}

