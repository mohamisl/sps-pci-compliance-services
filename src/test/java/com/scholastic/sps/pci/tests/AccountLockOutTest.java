package com.scholastic.sps.pci.tests;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Assert;
import org.junit.Test;

import com.jayway.restassured.path.xml.XmlPath;


import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class AccountLockOutTest extends BaseTest
{
	final String ENDPOINT_PCICOMPLIANCE_AUTHENTICATION="/AuthenticatePCI/processRemote";
	
	String clientID;
	String isSingleToken;
	String userName;
	String password;
	String xmlResponse;
	//bkukrin@juno1.com
	//ewood@juno1.com
	
	@Test
	public void ExistingUserAccountLockOutForInvalidPasswordTest()
	{
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"ewood@juno1.com\" ";
		password=" \"passe\" ";
		
		for(int i=0;i<20;i++)
		{
			ExtractableResponse<Response> existingUserLoginResponse=
								given()
										.contentType("application/x-www-form-urlencoded")
										.param("SPSWSXML",createQueryParam()).
								when()
										.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
								then()
										.statusCode(200)
										.contentType("application/xml")
										.extract();
			   
			xmlResponse=existingUserLoginResponse.xmlPath().getString("");
			System.out.println(xmlResponse);
		}	
			XmlPath xmlPath = new XmlPath(xmlResponse);
			xmlPath.setRoot("SchWS");
			System.out.println("*****"+xmlPath.get("attribute[0].@value"));
			Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("You have exceeded maximum number of invalid sign in attempts. For your security, your account has been locked and you must wait 30 minutes before attempting to sign in again. You may also reset your password to clear the lockout."));
	}
	
	public String createQueryParam()
	{
		String queryParam=
				"<SchWS>"+""
				+ "<attribute "+"name="+"\"clientID\" "+" value="+clientID+"/>"+
				"<attribute name="+"\"isSingleToken\" " +" value="+isSingleToken+"/>"+
				"<attribute name="+"\"userName\" " +" value="+userName+"/>"+
				"<attribute name="+"\"password\" " +" value="+password+"/>"+
				"</"+"SchWS"+">";
		return queryParam;
	}
}	