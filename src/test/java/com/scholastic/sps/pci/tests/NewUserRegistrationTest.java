package com.scholastic.sps.pci.tests;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.path.xml.XmlPath;

import static org.hamcrest.Matchers.notNullValue;

import org.junit.Assert;
import org.junit.Test;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class NewUserRegistrationTest extends BaseTest
{
	String clientID;
	String isSingleToken;
	String firstName;
	String lastName;
	String password;
	String email;
	String termsandCondition;
	String privacyPolicy;
	String salutation;
	
	final String ENDPOINT_PCICOMPLIANCE_AUTHENTICATION="/RegisterUserPCI/AuthenticatePCI/processRemote";
	
	/*
	 * Password PCI Compliance More Than 7 Character and Alpha Numeric.
	 */
	
	@Test
	public void newUserRegistrationWithPCICompliancePasswordTest() 
	{
		clientID="\"EMS\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"password123\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
		
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xxx=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xxx);
		xmlPath.setRoot("SchWS");
		System.out.println(xmlPath.get("attribute[1].@name"));
		System.out.println(xmlPath.get("attribute[1].@value"));
		Assert.assertTrue(xmlPath.get("attribute[1].@name").equals("spsid"));
		Assert.assertThat(xmlPath.get("attribute[1].@value"),notNullValue());
	}
	
	/*
	 * Password Less Than 7 Characters.
	 */
	
	@Test
	public void newUserRegistrationWithPasswordLessThan7characterTest()
	{
		clientID="\"COOL\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"pass\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
		
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println(xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Please enter a password that must be minimum of 7 and maximum of 30 characters, should contain at least an alphabetic letter and a number."));
		
	}
	
	/*
	 * Password More Than 7 Character But All Alphabet
	 */
	
	@Test
	public void newUserRegistrationWithPassword7characterButOnlyAlphabetTest()
	{
		clientID="\"EMS\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"password\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
		
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("Error Message :"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."));
	}
	
	/*
	 * Password More Than 7 Character But All Numeric.
	 */
	@Test
	public void newUserRegistrationWithPasswordLongButOnlyNumericTest()
	{
		clientID="\"EMS\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"123456789\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
								
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("Error Message :"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."));
	}
	
	/*
	 * Password Less Than 7 Characters But Alpha Numeric.
	 */
	
	@Test
	public void newUserRegistrationWithPasswordAlphaNumericButLessThan7CharacterTest()
	{
		clientID="\"EMS\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"pass1\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
		
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("Error Message :"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Please enter a password that must be minimum of 7 and maximum of 30 characters, should contain at least an alphabetic letter and a number."));
	}
	
	/*
	 *  Register New User With PCI Compliance Password When User Already Exist.
	 */
	
	@Test
	public void newUserRegistrationWhenUserAlreadyExistTest()
	{
		clientID="\"EMS\" ";
		firstName="\"SampleFirst\" ";
		lastName="\"SampleLast\" ";
		password="\"password3456\" ";
		termsandCondition="\"true\" ";
		privacyPolicy="\"true\" ";
		salutation="\"MR\" ";
		email="\"abora@juno1.com\" ";
		
		String queryParam=
				"<SchWS>"+
				"<attribute name="+"\"email\" "+"value="+email+"></attribute> "+
				"<attribute name="+"\"clientID\" "+"value="+clientID+"></attribute> "+
				"<attribute name="+"\"firstName\" "+"value="+firstName+"></attribute>"+
				"<attribute name="+"\"lastName\" "+"value="+lastName+"></attribute> "+
				"<attribute name="+"\"login\" "+"value="+'"'+getEmail()+ "\" ></attribute>"+
				"<attribute name="+"\"password\" "+"value="+password+"></attribute> "+
				"<attribute name="+"\"termsandCondition\" "+"value="+termsandCondition+"></attribute>"+
				"<attribute name="+"\"privacyPolicy\" "+"value="+privacyPolicy+"></attribute> "+
				"<attribute name="+"\"salutation\" "+"value="+salutation+"></attribute>"+
				"</"+"SchWS"+">";
							
		ExtractableResponse<Response> newUserRegistrationResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",queryParam).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(200)
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=newUserRegistrationResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("Error Message :"+xmlPath.get("attribute[0].@value").toString());
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("We're sorry, but an account already exists with that e-mail address. Please log-in or use different e-mail address."));
	}
	
	public String createQueryParam()
	{
		String queryParam=
				"<SchWS>"+
				"<attribute name="+"\"email\" "+"value="+'"'+getEmail()+ "\" ></attribute> "+
				"<attribute name="+"\"clientID\" "+"value="+clientID+"></attribute> "+
				"<attribute name="+"\"firstName\" "+"value="+firstName+"></attribute>"+
				"<attribute name="+"\"lastName\" "+"value="+lastName+"></attribute> "+
				"<attribute name="+"\"login\" "+"value="+'"'+getEmail()+ "\" ></attribute>"+
				"<attribute name="+"\"password\" "+"value="+password+"></attribute> "+
				"<attribute name="+"\"termsandCondition\" "+"value="+termsandCondition+"></attribute>"+
				"<attribute name="+"\"privacyPolicy\" "+"value="+privacyPolicy+"></attribute> "+
				"<attribute name="+"\"salutation\" "+"value="+salutation+"></attribute>"+
				"</"+"SchWS"+">";
		
		return queryParam;
	}
	
	
	public String getEmail()
	{
		email=System.currentTimeMillis()+"qa@sample.com";
		return email;
	}
}	